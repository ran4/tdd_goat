PATH := .:$(PATH)

all: install

geckodriver:
	./download_geckodriver.sh

.venv: .venv/bin/activate
.venv/bin/activate: requirements.txt
	test -d .venv || virtualenv --python=python3.5 .venv
	.venv/bin/pip install -Ur requirements.txt

test: geckodriver .venv/bin/activate
	. .venv/bin/activate && \
	    ./manage.py test

.PHONY: install
install: geckodriver .venv
	echo "Installed!"
