GZIP_NAME=geckodriver-v0.14.0-linux64.tar.gz
wget https://github.com/mozilla/geckodriver/releases/download/v0.14.0/$GZIP_NAME
tar -xvzf $GZIP_NAME
rm $GZIP_NAME
chmod +x geckodriver
export PATH=$(pwd):$PATH
